package com.project.iot.sensorreadings.viewModels

import com.project.iot.sensorreadings.api.TaskScheduler
import com.project.iot.sensorreadings.interfaces.navigation.NavigationManager
import com.project.iot.sensorreadings.models.enums.PageIndex

class MainViewModel(
    private val navigationManger: NavigationManager,
    private val taskScheduler: TaskScheduler
) {

    fun initialized() {
        taskScheduler.executeTasks()
        navigationManger.navigate(PageIndex.MainPage)
    }
}
