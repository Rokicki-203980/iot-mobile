package com.project.iot.sensorreadings.models.enums

enum class PageIndex {
    MainPage,
    SettingsPage
}
