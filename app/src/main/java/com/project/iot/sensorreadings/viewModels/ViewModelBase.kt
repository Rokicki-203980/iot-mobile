package com.project.iot.sensorreadings.viewModels

import com.project.iot.sensorreadings.utils.Handler
import kotlin.reflect.KProperty

abstract class ViewModelBase {

    protected class RaisePropertyChangedDelegate<T>(initialValue: T) {

        private var value: T = initialValue

        operator fun setValue(thisRef: ViewModelBase, property: KProperty<*>, value: T) {
            this.value = value
            thisRef.raisePropertyChanged(property.name)
        }

        operator fun getValue(thisRef: ViewModelBase, property: KProperty<*>): T {
            return value
        }
    }

    private val eventHandlers: MutableMap<Int, Handler<ViewModelBase, PropertyChangedEventArgs>> =
        mutableMapOf()

    val propertyChanged: Handler<ViewModelBase, PropertyChangedEventArgs>
        get() = eventHandlers.getOrPut(hashCode(), { Handler() })

    fun raisePropertyChanged(propertyName: String) {
        propertyChanged(this, PropertyChangedEventArgs(propertyName))
    }

    class PropertyChangedEventArgs(val propertyName: String = "")
}
