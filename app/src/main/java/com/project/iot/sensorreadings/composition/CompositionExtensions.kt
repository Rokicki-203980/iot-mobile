package com.project.iot.sensorreadings.composition

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.inSet
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.provider
import com.github.salomonbrys.kodein.setBinding
import com.project.iot.sensorreadings.api.ApiProvider
import com.project.iot.sensorreadings.api.MainApiCommunicator
import com.project.iot.sensorreadings.api.TaskScheduler
import com.project.iot.sensorreadings.interfaces.adapters.SettingsProvider
import com.project.iot.sensorreadings.interfaces.navigation.AuthorizationProvider
import com.project.iot.sensorreadings.interfaces.navigation.DispatcherAdapter
import com.project.iot.sensorreadings.interfaces.navigation.Schedulable
import com.project.iot.sensorreadings.interfaces.navigation.adapters.AndroidDispatcherAdapter
import com.project.iot.sensorreadings.interfaces.navigation.adapters.AndroidSettingsProvider
import com.project.iot.sensorreadings.interfaces.navigation.api.ApiCommunicator
import com.project.iot.sensorreadings.viewModels.DashboardViewModel
import com.project.iot.sensorreadings.viewModels.MainViewModel
import com.project.iot.sensorreadings.viewModels.SettingsViewModel
import pw.kmp.kodeinject.injectedSingleton

fun Kodein.registerDependencies(): Kodein {
    return Kodein {
        extend(this@registerDependencies)
        bind<ApiCommunicator>() with injectedSingleton<MainApiCommunicator>()
        bind<AuthorizationProvider>() with injectedSingleton<ApiProvider>()
        bind<SettingsProvider>() with injectedSingleton<AndroidSettingsProvider>()
        bind<DispatcherAdapter>() with injectedSingleton<AndroidDispatcherAdapter>()

        bind() from setBinding<Schedulable>()
        bind<Schedulable>().inSet() with provider { instance<DashboardViewModel>() }
        bind<TaskScheduler>() with provider { TaskScheduler(instance()) }
    }
}

fun Kodein.registerViewModels(): Kodein {
    return Kodein {
        extend(this@registerViewModels)
        bind<MainViewModel>() with injectedSingleton()
        bind<DashboardViewModel>() with injectedSingleton()
        bind<SettingsViewModel>() with injectedSingleton()
    }
}
