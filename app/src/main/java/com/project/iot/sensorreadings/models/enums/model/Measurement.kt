package com.project.iot.sensorreadings.models.enums.model

data class Measurement(
    val sound: Double,
    val light: Double,
    val temperature: Double,
    val humidity: Double
)
