package com.project.iot.sensorreadings.interfaces.navigation.adapters

import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.project.iot.sensorreadings.App
import com.project.iot.sensorreadings.interfaces.adapters.SettingsProvider
import com.project.iot.sensorreadings.utils.GsonConvert

class AndroidSettingsProvider : SettingsProvider {
    private val sharedPreferences: SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(App.current)

    override fun getInt(key: String): Int? {
        if (sharedPreferences.contains(key))
            return sharedPreferences.getInt(key, 0)
        return null
    }

    override fun putInt(key: String, value: Int) {
        val editor = sharedPreferences.edit()
        editor.putInt(key, value)
        editor.apply()
    }

    override fun getString(key: String): String? {
        return sharedPreferences.getString(key, null)
    }

    override fun putString(key: String, value: String) {
        val editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
    }

    override fun clear(key: String) {
        val editor = sharedPreferences.edit()
        editor.remove(key)
        editor.apply()
    }

    override fun <T : Any> putObject(key: String, value: T) {
        putString(key, GsonConvert.serializeObject(value))
    }

    override fun <T : Any> getObject(key: String): T? {
        val json = getString(key)
        return if (json == null) null else GsonConvert.deserializeObject(json)
    }
}
