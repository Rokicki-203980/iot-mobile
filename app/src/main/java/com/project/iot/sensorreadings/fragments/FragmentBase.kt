package com.project.iot.sensorreadings.fragments

import android.os.Bundle
import com.github.salomonbrys.kodein.TT
import com.jakewharton.rxbinding2.widget.TextViewAfterTextChangeEvent
import com.project.iot.sensorreadings.App
import com.project.iot.sensorreadings.viewModels.ViewModelBase
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import kotlin.reflect.KProperty

abstract class FragmentBase<TViewModel : ViewModelBase>(private val classToken: Class<TViewModel>) :
    NavigationFragmentBase() {

    protected lateinit var viewModel: TViewModel
        private set

    val bindings: MutableList<Disposable> = mutableListOf()

    private val ongoingChanges: MutableList<KProperty<*>> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        viewModel = App.kodeinContainer.Instance(TT(classToken))
        super.onCreate(savedInstanceState)
    }

    fun <T> createBinding(property: KProperty<T>): Observable<T> {
        return Observable.fromPublisher<T>({
            viewModel.propertyChanged += { _, e ->
                if (property !in ongoingChanges && e.propertyName == property.name) {
                    ongoingChanges.add(property)
                    it.onNext(property.call())
                    ongoingChanges.remove(property)
                }
            }
        })
    }

    protected fun TextViewAfterTextChangeEvent.getString(): String {
        return this.view().text.toString()
    }
}
