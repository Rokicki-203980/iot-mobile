package com.project.iot.sensorreadings.utils

import com.github.salomonbrys.kodein.instance
import com.project.iot.sensorreadings.App
import com.project.iot.sensorreadings.interfaces.adapters.SettingsProvider

class AppConfig {
    companion object {
        val instance = AppConfig()
        private val settingsProvider = App.kodeinContainer.instance<SettingsProvider>()

        const val appCenterSecret = "ebe745d9-8bdf-41b4-86ba-260073405d42"
    }

    var baseApiUrl: String
        get() {
            return settingsProvider.getString(this::baseApiUrl.name) ?: "http://10.0.2.2:8099"
        }
        set(value) = settingsProvider.putString(this::baseApiUrl.name, value)
    val configurationChanged: Handler<AppConfig, Any> = Handler()
}
