package com.project.iot.sensorreadings.viewModels

import android.util.Log
import com.github.kittinunf.result.Result
import com.jjoe64.graphview.GraphView
import com.project.iot.sensorreadings.R
import com.project.iot.sensorreadings.interfaces.navigation.DispatcherAdapter
import com.project.iot.sensorreadings.interfaces.navigation.NavigationManager
import com.project.iot.sensorreadings.interfaces.navigation.Schedulable
import com.project.iot.sensorreadings.interfaces.navigation.api.ApiCommunicator
import com.project.iot.sensorreadings.models.enums.PageIndex
import com.project.iot.sensorreadings.models.enums.model.Measurement
import com.project.iot.sensorreadings.utils.scheduleAtPeriod
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.async
import java.util.Random
import java.util.Timer


class DashboardViewModel(
    private val apiCommunicator: ApiCommunicator,
    private val navigationManger: NavigationManager,
    private val dispatcherAdapter: DispatcherAdapter
) : ViewModelBase(), Schedulable {
    private val refreshMeasurementPeriod: Long = 5000
    var temperatureList: MutableList<Double>? by RaisePropertyChangedDelegate(mutableListOf())


    override fun runTask(timer: Timer) {
        timer.scheduleAtPeriod(refreshMeasurementPeriod) {
            dispatcherAdapter.run({ refreshCurrentMeasurements() })
        }
    }

    fun navigatedTo() {
        dispatcherAdapter.run({ refreshCurrentMeasurements() })
    }

    var currentMeasurement: Measurement? by RaisePropertyChangedDelegate(null)

    private suspend fun refreshCurrentMeasurements() {
        val result = async(CommonPool) {
            apiCommunicator.getMeasurements()
        }.await()

        when (result) {
            is Result.Failure -> {
                currentMeasurement = null
                val zol = Random()
                val x = zol.nextDouble()
                var dododo: Measurement = Measurement(1.0,1.0,x,1.0)
                currentMeasurement = dododo
                temperatureList?.add(dododo.temperature)
                temperatureList = temperatureList
            }
            is Result.Success -> {
                Log.d("Pullowanie: ", "${result.value}")
                temperatureList?.add(result.value.temperature)
                currentMeasurement = result.value
                temperatureList = temperatureList
            }
        }
    }

    fun onSettingsViewButtonClick() {
        navigationManger.navigate(PageIndex.SettingsPage)
    }
}
