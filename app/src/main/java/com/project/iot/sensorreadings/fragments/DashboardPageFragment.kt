package com.project.iot.sensorreadings.fragments

import android.util.Log
import com.jjoe64.graphview.LegendRenderer
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import com.project.iot.sensorreadings.R
import com.project.iot.sensorreadings.viewModels.DashboardViewModel
import kotlinx.android.synthetic.main.dashboard_page.*


class DashboardPageFragment : FragmentBase<DashboardViewModel>(DashboardViewModel::class.java) {
    override val layoutResourceId: Int
        get() = R.layout.dashboard_page

    override fun navigatedTo() {
        super.navigatedTo()
        viewModel.navigatedTo()
    }

    override fun initBindings() {
        bindings.add(createBinding(viewModel::currentMeasurement).subscribe {
            if (it?.temperature == null) {
                Error.visibility
                Error.text = getString(R.string.lack_of_measurement)
            } else {
                LightTextValue.text = it.light.toString()
                TemperatureTextValue.text = it.temperature.toString()
                HumidityTextValue.text = it.humidity.toString()
                SoundTextValue.text = it.sound.toString()

            }
        })
        bindings.add(createBinding(viewModel::temperatureList).subscribe {
            if (it == null) {
                Error.visibility
                Error.text = getString(R.string.lack_of_measurement)
            } else {
                //SizeOfTable.text = it.size.toString()
                graph.removeAllSeries()
                val series: LineGraphSeries<DataPoint> = LineGraphSeries()
                for (i in 0..it.size-1)
                {
                    series.appendData(DataPoint(i.toDouble(),it[i]), true, 24)
                }
                series.title = "°C"

                graph.addSeries(series)
                graph.viewport.setMaxX(series.highestValueX+1.0)
                graph.viewport.setMinX(series.highestValueX-8.0)
                graph.viewport.setMaxY(1.0)
                graph.viewport.setMinY(0.0)
                graph.viewport.isScalable = true
                graph.viewport.isScrollable = true
                graph.viewport.setScalableY(true)
                graph.viewport.setScrollableY(true)
                graph.titleColor = R.color.colorPrimary
                graph.title = "CurrentTemperature"
                graph.titleTextSize = 60.toFloat()
                graph.legendRenderer.isVisible = true
                graph.legendRenderer.align = LegendRenderer.LegendAlign.TOP

                if(series.highestValueX < 10.0){graph.viewport.setMinX(0.0)}

            }
        })

        SettingsIcon.setOnClickListener { viewModel.onSettingsViewButtonClick() }
    }
}
