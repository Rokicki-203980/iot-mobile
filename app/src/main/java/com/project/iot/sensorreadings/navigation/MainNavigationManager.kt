package com.project.iot.sensorreadings.navigation

import android.support.v4.app.FragmentManager
import android.view.View
import com.project.iot.sensorreadings.fragments.NavigationFragmentBase
import com.project.iot.sensorreadings.interfaces.navigation.NavigationManager
import com.project.iot.sensorreadings.interfaces.navigation.PageProvider
import com.project.iot.sensorreadings.models.enums.PageIndex

class MainNavigationManager(
    private val pageDefinitions: Map<PageIndex, PageProvider<NavigationFragmentBase>>,
    private val rootView: View,
    private val fragmentManager: FragmentManager
) : NavigationManager {

    override fun wentBack() {
        (fragmentManager.fragments.last() as NavigationFragmentBase).navigatedTo()
    }

    private var firstNavigation: Boolean = true

    override fun goBack() {
        fragmentManager.popBackStack()
        fragmentManager.executePendingTransactions()
        wentBack()
    }

    override fun navigate(page: PageIndex) {

        val fragmentPage = pageDefinitions[page]!!.getPage()
        val transaction = fragmentManager
            .beginTransaction()
            .replace(rootView.id, fragmentPage)

        if (firstNavigation)
            firstNavigation = false
        else
            transaction.addToBackStack(null)

        transaction.commit()
        fragmentManager.executePendingTransactions()
        fragmentPage.navigatedTo()
    }
}
