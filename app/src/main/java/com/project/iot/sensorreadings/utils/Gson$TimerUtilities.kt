package com.project.iot.sensorreadings.utils

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.Timer
import java.util.TimerTask
import kotlin.concurrent.timerTask

fun <T> Gson.fromJson(json: String): T = this.fromJson<T>(json, object : TypeToken<T>() {}.type)

fun Timer.scheduleAtPeriod(period: Long, action: TimerTask.() -> Unit): TimerTask {
    val task = timerTask(action)
    schedule(task, 0, period)
    return task
}
