package com.project.iot.sensorreadings

import android.app.Application
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.provider
import com.project.iot.sensorreadings.activities.MainActivity
import com.project.iot.sensorreadings.composition.registerDependencies
import com.project.iot.sensorreadings.composition.registerViewModels
import com.project.iot.sensorreadings.interfaces.navigation.NavigationManager

class App : Application() {

    companion object {
        lateinit var kodeinContainer: Kodein
            internal set

        lateinit var current: App
            private set
    }

    init {
        current = this
    }

    override fun onCreate() {
        super.onCreate()

        val kodein = Kodein {}.registerDependencies().registerViewModels()

        kodeinContainer = Kodein.invoke {
            extend(kodein)
            bind<NavigationManager>() with provider { MainActivity.navigationManager }
        }
    }
}
