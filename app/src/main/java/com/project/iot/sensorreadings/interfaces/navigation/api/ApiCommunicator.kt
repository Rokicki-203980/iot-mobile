package com.project.iot.sensorreadings.interfaces.navigation.api

import com.github.kittinunf.result.Result
import com.project.iot.sensorreadings.models.enums.model.Measurement

interface ApiCommunicator {
    suspend fun getMeasurements(): Result<Measurement, Exception>
}
