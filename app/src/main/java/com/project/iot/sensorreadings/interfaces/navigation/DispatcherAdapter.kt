package com.project.iot.sensorreadings.interfaces.navigation

interface DispatcherAdapter {
    fun run(action: suspend () -> Unit)

    operator fun invoke(function: suspend () -> Unit) {
        run(function)
    }
}
