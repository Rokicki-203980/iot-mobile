package com.project.iot.sensorreadings.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.github.salomonbrys.kodein.instance
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes
import com.project.iot.sensorreadings.App
import com.project.iot.sensorreadings.R
import com.project.iot.sensorreadings.fragments.DashboardPageFragment
import com.project.iot.sensorreadings.fragments.NavigationFragmentBase
import com.project.iot.sensorreadings.fragments.SettingsPageFragment
import com.project.iot.sensorreadings.interfaces.navigation.NavigationManager
import com.project.iot.sensorreadings.interfaces.navigation.adapters.CachedPageProvider
import com.project.iot.sensorreadings.models.enums.PageIndex
import com.project.iot.sensorreadings.navigation.MainNavigationManager
import com.project.iot.sensorreadings.utils.AppConfig
import com.project.iot.sensorreadings.utils.Handler
import com.project.iot.sensorreadings.viewModels.MainViewModel
import kotlinx.android.synthetic.main.activity_main.RootView

class MainActivity : AppCompatActivity() {
    companion object {
        lateinit var navigationManager: NavigationManager
            private set
        lateinit var current: MainActivity
            private set

        private val TAG = this::class.qualifiedName
    }

    init {
        current = this
    }

    val onActivityResult: Handler<MainActivity, Triple<Int, Int, Intent?>> = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AppCenter.start(
            application,
            AppConfig.appCenterSecret,
            Analytics::class.java,
            Crashes::class.java
        )

        navigationManager = MainNavigationManager(
            mapOf(
                PageIndex.MainPage to CachedPageProvider<NavigationFragmentBase>({ DashboardPageFragment() }),
                PageIndex.SettingsPage to CachedPageProvider<NavigationFragmentBase>({ SettingsPageFragment() })
            ),
            RootView,
            supportFragmentManager
        )

        App.kodeinContainer.instance<MainViewModel>().initialized()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        onActivityResult.invoke(this, Triple(requestCode, resultCode, data))
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 0) {
            moveTaskToBack(true)
            return
        }
        super.onBackPressed()

        navigationManager.wentBack()
    }
}
