package com.project.iot.sensorreadings.utils

class Handler<TObj, TArg : Any> {
    private val handlers = arrayListOf<((TObj, TArg) -> Unit)>()

    operator fun plusAssign(handler: (TObj, TArg) -> Unit) {
        handlers.add(handler)
    }

    operator fun minusAssign(handler: (TObj, TArg) -> Unit) {
        handlers.remove(handler)
    }

    operator fun invoke(sender: TObj, e: TArg) {
        for (handler in handlers) {
            handler(sender, e)
        }
    }
}
