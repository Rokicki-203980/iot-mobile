package com.project.iot.sensorreadings.viewModels

import com.project.iot.sensorreadings.interfaces.navigation.NavigationManager
import com.project.iot.sensorreadings.utils.AppConfig

class SettingsViewModel(
    private val navigationManager: NavigationManager
) : ViewModelBase() {

    fun navigatedTo() {
        serverURI = AppConfig.instance.baseApiUrl
    }

    var serverURI: String by RaisePropertyChangedDelegate("")

    fun navigateBack() {
        navigationManager.goBack()
    }

    fun onConfirmSettingsButtonClick() {
        with(AppConfig.instance) {
            baseApiUrl = serverURI
            configurationChanged.invoke(this, Any())
        }
        navigationManager.goBack()
    }
}
